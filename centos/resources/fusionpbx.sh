#!/bin/sh

#move to script directory so all relative paths work
cd "$(dirname "$0")"

#includes
. ./config.sh
. ./colors.sh

#send a message
verbose "Installing FusionPBX"

#install dependencies
#yum -y install git
yum -y install ghostscript libtiff-devel libtiff-tools at

#forensics tools
wget https://forensics.cert.org/cert-forensics-tools-release-el7.rpm
rpm -Uvh cert-forensics-tools-release*rpm
yum -y --enablerepo=forensics install lame

#get the branch
if [ .$system_branch = .'master' ]; then
	verbose "Using master"
	BRANCH=""
else
	FUSION_MAJOR=4
	FUSION_MINOR=2
	FUSION_VERSION=$FUSION_MAJOR.$FUSION_MINOR
	verbose "Using version $FUSION_VERSION"
	BRANCH="-b $FUSION_VERSION"
fi

#get the source code
git clone $BRANCH https://gitlab.com/soonjoin/fusionpbx.git /var/www/fusionpbx

#send a message
verbose "FusionPBX Installed"
